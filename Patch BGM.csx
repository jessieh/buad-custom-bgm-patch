/*
 * PatchBGM
 * Patches the Buck Up And Drive! BGM (bgm001 in the game files)
 * so that the game will stream it from an external audio file
 * instead of using the embedded audio.
 */

EnsureDataLoaded();

// Sound information

const string target_sound_name = "bgm001";
const string sound_file_name = "bgm.ogg";

// Locate the BGM and apply the patch
    
for ( var i = 0; i < Data.Sounds.Count; i++ )
{
    if ( Data.Sounds[i].Name.Content == target_sound_name )
    {

        UndertaleSound target_sound = Data.Sounds[i];
        
        target_sound.Flags = UndertaleSound.AudioEntryFlags.Regular;
        target_sound.Type = Data.Strings.MakeString( ".ogg" ); // GM:S only supports external streaming from OGG files
        target_sound.File = Data.Strings.MakeString( sound_file_name );
        target_sound.AudioFile = null;
        target_sound.AudioID   = -1;
        target_sound.Preload = true;

        ScriptMessage( $"{ target_sound_name } patched!\n\nIf there are any issues, have Steam verify the integrity of your game files." );
        ScriptMessage( $"You may now place your own music in the game directory.\n\n(The file must be named '{ sound_file_name }')" );
        return;

    }
}
    
    
ScriptMessage( $"Failed to find { target_sound_name } in the game files.\n\nPatch not applied." );