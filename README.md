# BUAD Custom BGM Patch

A patch for [Buck Up and Drive!](https://store.steampowered.com/app/1714590) that modifies the in-game BGM to stream from an external audio file in the game folder, so that the player may supply their own music.

## Usage

Apply `Patch BGM.csx` using [BUAD Patcher](https://gitlab.com/jessieh/buad-patcher) and place your audio file (named `bgm.ogg`) in your game folder.

(Typically: `C:\Program Files (x86)\Steam\steamapps\common\Buck Up And Drive!`)